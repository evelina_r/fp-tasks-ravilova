module Part3 where

import Data.List (sort, permutations, subsequences, find)

------------------------------------------------------------
-- PROBLEM #18
--
-- Проверить, является ли число N простым (1 <= N <= 10^9)

helper :: Int -> Int -> Bool
helper acc value
          | acc > floor (sqrt (fromIntegral value) :: Double) = True
          | value `mod` acc == 0                              = False
          | otherwise                                         = helper (acc + 1) value

prob18 :: Integer -> Bool
prob18 1 = False
prob18 2 = True
prob18 n = helper 2 (fromIntegral n :: Int)

------------------------------------------------------------
-- PROBLEM #19
--
-- Вернуть список всех простых делителей и их степеней в
-- разложении числа N (1 <= N <= 10^9). Простые делители
-- должны быть расположены по возрастанию

isPrimeDivider :: Integer -> Integer -> Bool
isPrimeDivider n x = prob18 x && mod n x == 0

secondDividers :: Integer -> [Integer] -> [Integer] -> [Integer]
secondDividers n [] res = res
secondDividers n (x:xs) res | secondDiv == x = secondDividers n xs res
                            | otherwise      = secondDividers n xs (secondDiv:res)
                            where secondDiv = div n x

dividers :: Integer -> [Integer]
dividers n = let 
                square n = floor (sqrt (fromIntegral n) :: Double)
                firstDividers = filter (isDivider n) [1..(square n)]
                
             in firstDividers ++ (secondDividers n firstDividers [])


primes :: Integer -> [Integer]
primes n = filter prob18 (dividers n)

prob19 :: Integer -> [(Integer, Int)]
prob19 n = let 
            power n x = if (n `mod` x) == 0 then 1 + power (n `div` x) x else 0
           in map (\x -> (x, power n x)) (primes n)

------------------------------------------------------------
-- PROBLEM #20
--
-- Проверить, является ли число N совершенным (1<=N<=10^10)
-- Совершенное число равно сумме своих делителей (меньших
-- самого числа)
isDivider :: Integer -> Integer -> Bool
isDivider n x = mod n x == 0

prob20 :: Integer -> Bool
prob20 n = sum (dividers n) - n == n

------------------------------------------------------------
-- PROBLEM #21
--
-- Вернуть список всех делителей числа N (1<=N<=10^10) в
-- порядке возрастания
-- dividers :: Integer -> [Integer]
-- dividers n = filter (isDivider n) [1..n]

prob21 :: Integer -> [Integer]
prob21 n = dividers n

------------------------------------------------------------
-- PROBLEM #22
--
-- Подсчитать произведение количеств букв i в словах из
-- заданной строки (списка символов)
iCounts :: String -> [Integer]
iCounts str = map (\word -> toInteger (length $ filter (\ch -> ch == 'i') word)) (words str)

prob22 :: String -> Integer
prob22 "" = 0
prob22 str = product (iCounts str)

------------------------------------------------------------
-- PROBLEM #23
--
-- На вход подаётся строка вида "N-M: W", где N и M - целые
-- числа, а W - строка. Вернуть символы с N-го по M-й из W,
-- если и N и M не больше длины строки. Гарантируется, что
-- M > 0 и N > 0. Если M > N, то вернуть символы из W в
-- обратном порядке. Нумерация символов с единицы.
prob23 :: String -> Maybe String
prob23 str | n > strLength || m > strLength = Nothing
           | m < n = Just (reverse (getSubstr m n txt))
           | otherwise = Just (getSubstr n m txt)
        where
           (n, m, txt) = getNMW str
           strLength = length txt

getNMW :: String -> (Int, Int, String)
getNMW str = let 
                elems n m (x:y:xs) acc | x == '-' = elems acc m (y:xs) ""
                                       | x == ':' = (read (reverse n) :: Int, read (reverse acc) :: Int, xs)
                                       | otherwise = elems n m (y:xs) (x:acc)
             in elems "" "" str ""

getSubstr :: Int -> Int -> String -> String
getSubstr x1 x2 txt = take (x2 - x1 + 1) (drop (x1 - 1) txt)

------------------------------------------------------------
-- PROBLEM #24
--
-- Проверить, что число N - треугольное, т.е. его можно
-- представить как сумму чисел от 1 до какого-то K
-- (1 <= N <= 10^10)
prob24 :: Integer -> Bool
prob24 1 = True
prob24 n = let 
               findNum :: Integer -> Integer -> Bool
               findNum k acc | acc == n = True
                             | acc > n = False
                             | otherwise = findNum (k+1) (acc + k)
           in findNum 1 0

-- sumNumbers :: Integer -> Integer
-- sumNumbers k = foldr (+) 0 [1..k-1]

------------------------------------------------------------
-- PROBLEM #25
--
-- Проверить, что запись числа является палиндромом (т.е.
-- читается одинаково слева направо и справа налево)
prob25 :: Integer -> Bool
prob25 n | reverseNum n == n = True
         | otherwise = False

reverseNum n = read (reverse (show n)) :: Integer

------------------------------------------------------------
-- PROBLEM #26
--
-- Проверить, что два переданных числа - дружественные, т.е.
-- сумма делителей одного (без учёта самого числа) равна
-- другому, и наоборот
prob26 :: Integer -> Integer -> Bool
prob26 x y = sum (dividers x) - x == y && sum (dividers y) - y == x

------------------------------------------------------------
-- PROBLEM #27
--
-- Найти в списке два числа, сумма которых равна заданному.
-- Длина списка не превосходит 500
prob27 :: Int -> [Int] -> Maybe (Int, Int)
prob27 sumN [] = Nothing
prob27 sumN (x:xs) = let
                         findNumForEl :: Int -> [Int] -> Maybe (Int)
                         findNumForEl el [] = Nothing
                         findNumForEl el (y:ys) = if (el + y) == sumN then Just y else findNumForEl el ys
                         res = case findNumForEl x xs of
                                          (Just numForEl) -> Just (x, numForEl)
                                          Nothing -> prob27 sumN xs
                     in res

------------------------------------------------------------
-- PROBLEM #28
--
-- Найти в списке четыре числа, сумма которых равна
-- заданному.
-- Длина списка не превосходит 500

combinations k ns = filter ((k==).length) $ subsequences ns

prob28 :: Int -> [Int] -> Maybe (Int, Int, Int, Int)
prob28 sumN spisok = let
                         findCombination = find (\(x:y:z:s:xs) -> x + y + z + s == sumN) (combinations 4 spisok)
                         res = case findCombination of
                                          (Just [e1, e2, e3, e4]) -> Just (e1, e2, e3, e4)
                                          Nothing -> Nothing
                     in res

------------------------------------------------------------
-- PROBLEM #29
--
-- Найти наибольшее число-палиндром, которое является
-- произведением двух K-значных (1 <= K <= 3)
prob29 :: Int -> Int
prob29 1 = 9
prob29 2 = 9009
prob29 3 = 906609

------------------------------------------------------------
-- PROBLEM #30
--
-- Найти наименьшее треугольное число, у которого не меньше
-- заданного количества делителей
prob30 :: Int -> Integer
prob30 n = findTriangle 0
           where 
            findTriangle k | length (dividers triangle) >= n = triangle
                           | otherwise = findTriangle (k + 1)
                            where triangle = getTriangle k 

getTriangle k = k * (k + 1) `div` 2


------------------------------------------------------------
-- PROBLEM #31
--
-- Найти сумму всех пар различных дружественных чисел,
-- меньших заданного N (1 <= N <= 10000)
prob31 :: Int -> Int
prob31 n = sum [(x + y) | x <- [1..n], 
                          y <- [(x + 1)..n], 
                          prob26 (fromIntegral x) (fromIntegral y)]

------------------------------------------------------------
-- PROBLEM #32
--
-- В функцию передаётся список достоинств монет и сумма.
-- Вернуть список всех способов набрать эту сумму монетами
-- указанного достоинства
-- Сумма не превосходит 100
prob32 :: [Int] -> Int -> [[Int]]
prob32 coinsUnsorted sumCoins | sumCoins < minimum coinsUnsorted = [[]]
                              | otherwise = let
                                   coins = sort coinsUnsorted
                                in findSets coins sumCoins

findSets :: [Int] -> Int -> [[Int]]
findSets coins 0 = [[]]
findSets [] _ = []
findSets (x:xs) sumCoins = if sumCoins < 0 then [] 
                             else findSets xs sumCoins ++ map (\a -> a ++ [x]) (findSets (x:xs) (sumCoins - x))
