module Part2 where

import Part2.Types

import Data.Maybe

------------------------------------------------------------
-- PROBLEM #6
--
-- Написать функцию, которая преобразует значение типа
-- ColorLetter в символ, равный первой букве значения
prob6 :: ColorLetter -> Char
prob6 RED = 'R'
prob6 GREEN = 'G'
prob6 BLUE = 'B'

------------------------------------------------------------
-- PROBLEM #7
--
-- Написать функцию, которая проверяет, что значения
-- находятся в диапазоне от 0 до 255 (границы входят)
prob7 :: ColorPart -> Bool
prob7 x = prob9 x >= 0 && prob9 x < 256 

------------------------------------------------------------
-- PROBLEM #8
--
-- Написать функцию, которая добавляет в соответствующее
-- поле значения Color значение из ColorPart
updateColor :: ColorPart -> Color -> Color
updateColor (Red newColor) color = color {red = red color + newColor} 
updateColor (Green newColor) color = color {green = green color + newColor} 
updateColor (Blue newColor) color = color {blue = blue color + newColor} 

prob8 :: Color -> ColorPart -> Color
prob8 c cp = updateColor cp c

------------------------------------------------------------
-- PROBLEM #9
--
-- Написать функцию, которая возвращает значение из
-- ColorPart
prob9 :: ColorPart -> Int
prob9 (Red x) = x
prob9 (Green x) = x
prob9 (Blue x) = x

------------------------------------------------------------
-- PROBLEM #10
--
-- Написать функцию, которая возвращает компонент Color, у
-- которого наибольшее значение (если такой единственный)
prob10 :: Color -> Maybe ColorPart
prob10 c | red c > green c && red c > blue c = Just (Red $ red c) 
         | green c > red c && green c > blue c = Just (Green $ green c)
         | blue c > red c && blue c > green c = Just (Blue $ blue c)
         | otherwise = Nothing

------------------------------------------------------------
-- PROBLEM #11
--
-- Найти сумму элементов дерева
prob11 :: Num a => Tree a -> a
prob11 (Tree l x r) = let
        left = case l of
                  Just l -> prob11 l
                  Nothing -> 0
        right = case r of
                  Just r -> prob11 r
                  Nothing -> 0
        in left + x + right


------------------------------------------------------------
-- PROBLEM #12
--
-- Проверить, что дерево является деревом поиска
-- (в дереве поиска для каждого узла выполняется, что все
-- элементы левого поддерева узла меньше элемента в узле,
-- а все элементы правого поддерева -- не меньше элемента
-- в узле)
abc prev (Tree l x r) side flag | not flag = False
                                | side == "left" && prev > x || side == "right" && prev <= x = let
                                left = case l of
                                          Just l -> abc x l "left" True
                                          Nothing -> True
                                right = case r of
                                          Just r -> abc x r "right" True
                                          Nothing -> True
                                in left && right
                                | otherwise = False

prob12 :: Ord a => Tree a -> Bool

prob12 (Tree l x r) = let
        left = case l of
                  Just l -> abc x l "left" True
                  Nothing -> True 
        right = case r of
                  Just r -> abc x r "right" True
                  Nothing -> True
        in left && right

------------------------------------------------------------
-- PROBLEM #13
--
-- На вход подаётся значение и дерево поиска. Вернуть то
-- поддерево, в корне которого находится значение, если оно
-- есть в дереве поиска; если его нет - вернуть Nothing



prob13 :: Ord a => a -> Tree a -> Maybe (Tree a)
prob13 n (Tree l x r) | x == n = Just (Tree l x r)
            | x /= n && isNothing l && isNothing r = Nothing
                      | otherwise = let
                  left = case l of
                            Just l -> prob13 n l
                            Nothing -> Nothing
                  right = case r of
                            Just r -> prob13 n r
                            Nothing -> Nothing
                         in if isJust left then left else right



------------------------------------------------------------
-- PROBLEM #14
--
-- Заменить () на числа в порядке обхода "правый, левый,
-- корень", начиная с 1
enum :: Int -> Tree () -> (Int, Maybe (Tree Int))
enum i (Tree l () r) = let
                        right = case r of
                                      Just r -> enum i r
                                      Nothing -> (i, Nothing)
                        left = case l of
                                    Just l -> enum (fst right) l
                                    Nothing -> (i, Nothing)
                        node = if isNothing (snd left) then fst right else fst left
                 
            in (node + 1, Just (Tree (snd left) node (snd right)))

prob14 :: Tree () -> Tree Int
prob14 tree = fromJust $ snd (enum 1 tree)


------------------------------------------------------------
-- PROBLEM #15
--
-- Выполнить вращение дерева влево относительно корня
-- (https://en.wikipedia.org/wiki/Tree_rotation)
prob15 :: Tree a -> Tree a
prob15 (Tree t1 x Nothing) = (Tree t1 x Nothing)
prob15 (Tree t1 x (Just (Tree t2 y t3))) = Tree (Just (Tree t1 x t2)) y t3

------------------------------------------------------------
-- PROBLEM #16
--
-- Выполнить вращение дерева вправо относительно корня
-- (https://en.wikipedia.org/wiki/Tree_rotation)
prob16 :: Tree a -> Tree a
prob16 (Tree Nothing x t3) = (Tree Nothing x t3)
prob16 (Tree (Just (Tree t1 y t2)) x t3) = Tree t1 y (Just (Tree t2 x t3))

------------------------------------------------------------
-- PROBLEM #17
--
-- Сбалансировать дерево поиска так, чтобы для любого узла
-- разница высот поддеревьев не превосходила по модулю 1
-- (например, преобразовать в полное бинарное дерево)
prob17 :: Tree a -> Tree a
prob17 (Tree t1 y t2) | abs (sy) < 2         = Tree t1 y t2
                      | sy == 2 && st1 /= -1 = prob16 (Tree t1 y t2)
                      | sy == 2 && st1 == -1 = prob16 (Tree (Just (prob15 (fromJust t1))) y t2)
                      | sy == -2 && st2 /= 1 = prob15 (Tree t1 y t2)
                      | sy == -2 && st2 == 1 = prob15 (Tree t1 y (Just (prob16 (fromJust t2))))
                      | sy > 2 = prob17 $ (prob16 (Tree t1 y t2))
                      | sy < -2 = prob17 $ (prob15 (Tree t1 y t2))
                        where
                         sy  = slope $ Just (Tree t1 y t2)
                         st1 = slope t1
                         st2 = slope t2


slope :: Maybe (Tree a) -> Int
slope Nothing = 0
slope (Just (Tree t1 x t2)) = (height t1) - (height t2)

height :: Maybe (Tree a) -> Int
height Nothing = 0
height (Just (Tree t1 x t2)) = 1 + (max (height t1) (height t2))
